<?php

namespace App\Http\Controllers;

use App\EpisodeRepository;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Http\Response;

class EpisodeController extends Controller
{
    /**
     * The episode repository instance.
     *
     * @var EpisodeRepository
     */
    protected $episodes;

    /**
     * Create a new controller instance.
     *
     * @param  EpisodeRepository  $tasks
     * @return void
     */
    public function __construct(EpisodeRepository $episodes)
    {
        $this->episodes = $episodes;
    }

    /**
     * Display a list of all episodes.
     *
     * @param  Request  $request
     * @return Response
     */
    public function index(Request $request)
    {
        /*$tasks = $request->user()->tasks()->get();
        return view('tasks.index', [
            'tasks' => $tasks,
        ]);*/

        $episodes = $this->episodes->getAll();
        return view('episodes', [
            'episodes' => $episodes,
        ]);
    }
}
