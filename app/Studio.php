<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Studio extends Model
{
    /**
     * Get all of the episodes for the studio.
     */
    public function episodes()
    {
        return $this->hasMany(Episode::class);
    }
}
