<?php

namespace App\Console\Commands;

use App\Episode;
use App\Studio;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Sunra\PhpSimple\HtmlDomParser;

class GrabEpisodes extends Command
{
    const LOSTFILM = 1;
    const NEWSTUDIO = 2;
    const KUBIC3 = 3;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'episode:grab 
                            {studio : The ID of the studio}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Grab episodes from studio site';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $startTime = microtime(true);

        $studioID = $this->argument('studio');
        $studio = Studio::find($studioID);

        $this->info('Grab episodes of studio:' . ($studio ? $studio->name : $studio));

        $newEpisodes = 0;

        switch ($studioID) {
            case self::LOSTFILM:
                $newEpisodes = $this->grabLostFilm($studio);
                break;
            case self::NEWSTUDIO:
                $newEpisodes = $this->grabNewStudio($studio);
                break;
            case self::KUBIC3:
                $newEpisodes = $this->grabKubic($studio);
                break;
        }

        $finishTime = microtime(true);
        $elapsedTime = $finishTime - $startTime;

        $this->info(sprintf('New episodes: %s ', $newEpisodes));
        $this->info(sprintf('[INFO] Elapsed time: %.2f ms', $elapsedTime * 1000));
    }

    private function grabLostFilm(Studio $studio)
    {
        $result = 0;

        $dom = HtmlDomParser::file_get_html($studio->list_url);
        $this->info($studio->list_url);

        $container = $dom->find('div.mid div.content_body', 0);
        $seasonEpisodes = $container->find('div[style^=float:right]');
        $serialNames = $container->find('span[style^=font-family:arial]');
        //$images = $container->find('img');
        $dates = $container->find('b');
        $urls = $container->find('a.a_details');

        $len = count($seasonEpisodes);
        $this->info(sprintf('Length: %s ', $len));

        for ($i = 0; $i < $len; $i++) {
            $url = $urls[$i]->href;

            $numRows = DB::table('episodes')->where('url', $url)->count();
            if ($numRows != 0) {
                break;
            }

            $this->info(sprintf('url: %s ', $url));

            $seasonEpisode = explode('.', $seasonEpisodes[$i]->plaintext);
            $date = $dates[$i * 3 + 1]->plaintext;
            $image = HtmlDomParser::file_get_html($studio->main_url . $url)->find('div.mid div img', -1)->src;
            if (strpos($image, 'http') === false) {
                $image = $studio->main_url . $image;
            }

            $episode = new Episode();
            $episode->name = $serialNames[$i]->plaintext;
            $episode->season = intval($seasonEpisode[0]);
            $episode->episode = count($seasonEpisode) > 1 ? intval($seasonEpisode[1]) : '';
            $episode->url = $url;
            $episode->image = $image;
            $episode->published_at = new \DateTime($date);
            $episode->studio_id = $studio->id;
            $episode->save();

            $result++;
        }

        return $result;
    }

    private function grabNewStudio(Studio $studio)
    {
        $result = 0;

        $dom = HtmlDomParser::file_get_html($studio->list_url);
        $this->info($studio->list_url);

        $containers = $dom->find('#sideLeft div.accordion-inner div.date, #sideLeft div.accordion-inner div.torrent');

        foreach ($containers as $container) {

            if ($container->class == 'date') {
                $date = $container->plaintext;
                continue;
            }

            $url = $container->find('div.pull-right a', 1)->href;

            $numRows = DB::table('episodes')->where('url', $url)->count();
            if ($numRows != 0) {
                break;
            }

            $this->info(sprintf('url: %s ', $url));

            $seasonEpisode = $container->find('div.tdesc', 0)->plaintext;
            preg_match_all("(\d+)",
                $seasonEpisode,
                $parts, PREG_PATTERN_ORDER);
            $page = HtmlDomParser::file_get_html($studio->main_url . $url);
            $time = explode(' ', $page->find('div.viewtopic a.small', 0)->plaintext)[1];

            $episode = new Episode();
            $episode->name = trim($container->find('div.ttitle', 0)->plaintext);
            $episode->season = intval($parts[0][0]);
            $episode->episode = count($parts[0]) > 2 ? $parts[0][1] : '';
            $episode->url = $url;
            $episode->image = $page->find('div.post_wrap var.postImg', 0)->title;
            $episode->published_at = new \DateTime($date . ' ' . $time);
            $episode->studio_id = $studio->id;
            $episode->save();

            $result++;
        }

        return $result;
    }

    private function grabKubic(Studio $studio)
    {
        $result = 0;

        $dom = HtmlDomParser::file_get_html($studio->list_url);
        $this->info($studio->list_url);

        $containers = $dom->find('div#allEntries div.list-item');

        foreach ($containers as $container) {

            $url = $container->find('a', 0)->href;

            $numRows = DB::table('episodes')->where('url', $url)->count();
            if ($numRows != 0) {
                break;
            }

            $this->info(sprintf('url: %s ', $url));

            $infos = explode('<br>', $container->find('div.name span', 0)->innertext);
            preg_match_all("([0-9\-]+)",
                $infos[1],
                $parts, PREG_PATTERN_ORDER);

            $dateText = $container->find('div.info span.rating', 0)->plaintext;
            if ($dateText == 'Сегодня') {
                $date = new \DateTime();
            } elseif ($dateText == 'Вчера') {
                $date = new \DateTime();
                $date->add(\DateInterval::createFromDateString('yesterday'));
            } else {
                $date = new \DateTime($dateText);
            }

            $episode = new Episode();
            $episode->name = $infos[0];
            $episode->season = intval($parts[0][0]);
            $episode->episode = count($parts[0]) > 2 ? $parts[0][1] : '';
            $episode->url = $url;
            $episode->image = $container->find('img', 0)->src;
            $episode->published_at = $date;
            $episode->studio_id = $studio->id;
            $episode->save();

            $result++;
        }

        return $result;
    }
}
