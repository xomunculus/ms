<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Episode extends Model
{
    /**
     * Get the user that owns the task.
     *
     * @return Studio
     */
    public function studio()
    {
        return $this->belongsTo(Studio::class);
    }
}
