<?php
/**
 * Created by IntelliJ IDEA.
 * User: undkit
 * Date: 28.09.16
 * Time: 16:07
 */

namespace App;


class EpisodeRepository
{
    public function getAll()
    {
        return Episode::with('studio')->orderBy('published_at', 'desc')->paginate(10);
    }
}