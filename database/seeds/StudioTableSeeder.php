<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StudioTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('studios')->insert([
            'name' => 'LostFilm',
            'main_url' => 'http://www.lostfilm.tv',
            'list_url' => 'http://www.lostfilm.tv/browse.php',
        ]);

        DB::table('studios')->insert([
            'name' => 'NewStudio',
            'main_url' => 'http://newstudio.tv',
            'list_url' => 'http://newstudio.tv/'
        ]);

        DB::table('studios')->insert([
            'name' => 'NewStudio',
            'main_url' => 'http://kubik3.ru',
            'list_url' => 'hhttp://kubik3.ru/load/ozvuchka/kubik_v_kube/277',
        ]);
    }
}
