<?php

use Illuminate\Database\Seeder;

class EpisodeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('episodes')->insert([
            'studio_id' => '1',
            'name' => 'Бойтесь ходячих мертвецов',
            'season' => '2',
            'episode' => '13',
            'url' => 'browse.php?cat=252',
            'image' => '/Static/icons/cat_fear_the_walking_dead.jpeg',
            'published_at' => new DateTime(),
        ]);
        DB::table('episodes')->insert([
            'studio_id' => '1',
            'name' => 'Бойтесь ходячих мертвецов',
            'season' => '2',
            'episode' => '12',
            'url' => 'browse.php?cat=253',
            'image' => '/Static/icons/cat_fear_the_walking_dead.jpeg',
            'published_at' => new DateTime(),
        ]);
        DB::table('episodes')->insert([
            'studio_id' => '1',
            'name' => 'Бойтесь ходячих мертвецов',
            'season' => '2',
            'episode' => '11',
            'url' => 'browse.php?cat=254',
            'image' => '/Static/icons/cat_fear_the_walking_dead.jpeg',
            'published_at' => new DateTime(),
        ]);
    }
}
