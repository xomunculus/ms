<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEpisodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('studios', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('list_url');
            $table->string('main_url');
        });

        Schema::create('episodes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('studio_id')->unsigned()->index();
            $table->string('name');
            $table->integer('season');
            $table->string('episode');
            $table->string('url')->unique();
            $table->string('image');
            $table->dateTime('published_at');
            $table->timestamps();

            $table->foreign('studio_id')
                ->references('id')->on('studios')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('episodes');
        Schema::dropIfExists('studios');
    }
}
