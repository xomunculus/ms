@extends('layouts.app')
@section('content')
    @foreach ($episodes as $episode)
        <div class="row well episode">
            <div class="col-xs-3">
                <a href="{{ $episode->studio->main_url }}{{ $episode->url }}"><img src="{{--{{ $episode->studio->main_url }}--}}{{ $episode->image }}"/></a>
            </div>

            <div class="col-xs-9">
                <div class="meta">
                    <small><i class="fa fa-calendar"></i>{{$episode->published_at }}</small>
                    <a href="{{ $episode->studio->main_url }}" class="author"><i class="fa fa-film"></i> {{ $episode->studio->name }}</a>
                </div>
                <h4>{{ $episode->name }}</h4>
                <p>Сезон {{ $episode->season }}, {{ $episode->episode ? 'Серия '.$episode->episode : 'Все серии' }}</p>
                <a href="{{ $episode->studio->main_url }}{{ $episode->url }}" class="btn btn-primary">Подробнее</a>
            </div>
        </div>
    @endforeach

    <div class="row text-center">
        {{ $episodes->links('vendor.pagination.bootstrap-4') }}
    </div>
@endsection